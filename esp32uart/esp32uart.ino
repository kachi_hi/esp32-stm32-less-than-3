#define LED_BUILTIN 2
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  Serial2.begin(115200);
}


void loop() {
  Serial2.write(1);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  Serial2.write(0);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}
